# Appendix: Software in Use

## Software used internally in Event Data

The Crossref Event Data system has a number of components. All parts of the system that are used to generate or distribute Events, Evidence, Artifacts are open source. Whilst you will probably never want to run any of this software, it may help with the interpretation of the data.

Crossref Event Data uses a collection of software. It is all open source and is document on our [Knowledge Database](https://crossref.gitlab.io/knowledge_base/products/event-data/) pages.

## Data flow through the system

This chart shows the flow of data through the system. A [PDF](../images/ced-data-flow.pdf) shows more detail.

<img src='../images/ced-data-flow.png' alt='Event Data Flows' class='img-responsive'>


